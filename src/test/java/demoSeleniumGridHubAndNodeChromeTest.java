import org.testng.annotations.*;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.*;
import java.net.URL;
import java.util.List;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.concurrent.TimeUnit;

public class demoSeleniumGridHubAndNodeChromeTest {
	WebDriver driver;
//String hubURL = "http://192.168.10.41:4444";
String hubURL = "http://localhost:4444";
	// http://164.52.192.223/
	//String hubURL = "http://164.52.192.223:4444";

  @BeforeMethod
  public void beforeMethod() throws Exception {
	/*
	DesiredCapabilities caps = new DesiredCapabilities();
      caps.setBrowserName("firefox");
      driver = new RemoteWebDriver(new URL(hubURL), caps);
*/
/*
		System.setProperty("JAEGER_SERVICE_NAME", "selenium-java-client");
		System.setProperty("JAEGER_AGENT_HOST","http://164.52.192.223/");
		System.setProperty("JAEGER_AGENT_PORT","14250");
*/

		System.setProperty("otel.traces.exporter", "jaeger");
		System.setProperty("otel.exporter.jaeger.endpoint", "http://35.243.230.249:14250");
		System.setProperty("otel.resource.attributes", "service.name=selenium-java-client");

		ImmutableCapabilities capabilities = new ImmutableCapabilities("browserName", "chrome");
		driver = new RemoteWebDriver(new URL(hubURL), capabilities);
	/*DesiredCapabilities caps = new DesiredCapabilities();
			caps.setBrowserName("chrome");

		driver = new RemoteWebDriver(new URL(hubURL), caps);

driver = new RemoteWebDriver(
                new URL("http://192.168.10.41:4444"),
               new ChromeOptions());
*/
  }

  @Test
  public void launchApplication() throws Exception{
	  driver.get("https://seleniumsummit21.agiletestingalliance.org/");
	  //find all links on the home page and print information
	  List<WebElement> links = driver.findElements(By.xpath("//a"));
	  for(WebElement link: links) {
		  System.out.println(link.getText());
		  System.out.println(link.getAttribute("href"));
	  }
  }

  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }

}
